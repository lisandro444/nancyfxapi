﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NancyApiTest
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // All of our other middleware
            // ...

            // Nancy
            app.UseNancy();
        }
    }
}