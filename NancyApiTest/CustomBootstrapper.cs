﻿using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NancyApiTest
{
    public class CustomBootstrapper: DefaultNancyBootstrapper
    {
         
        protected override void ConfigureApplicationContainer(TinyIoCContainer container) {

            base.ConfigureApplicationContainer(container);

        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
            StaticConfiguration.DisableErrorTraces = false;
        }

        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            
        }
    }
}