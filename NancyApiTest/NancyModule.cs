﻿using Nancy;
using NancyApiTest.Context;
using NancyApiTest.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NancyApiTest
{
    public class NancyModuleProduct: Nancy.NancyModule
    {      
        public NancyModuleProduct()
        {           
                Get["/"] = _ => "Hello World!";

                Get["/api/productlist"] = _ =>
                {
                    List<Product> products;
                    using (DataContext context = new DataContext())
                    {
                        products = context.Product.ToList();
                    }
                    return Response.AsJson(products);
                };         
        }
    }
}